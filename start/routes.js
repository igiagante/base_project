'use strict';

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');
const Config = use('Config');
const Env = use('Env');

const api = Env.get('API_BASE_URL');

Route.get('swagger.json', ({ response }) => {
  response.type('application/json').send(Config.get('swagger'));
});

// Security routes

Route.group(() => {
  Route.post('login', 'SecurityController.login');
}).prefix(`${api}/security`);

// User routes

Route.group(() => {
  Route.get('self', 'UserController.self');
})
  .prefix(`${api}/users`)
  .middleware('auth');

// Image routes

Route.group(() => {
  Route.post('upload', 'ImageController.upload');
}).prefix(`${api}/images`);
// .middleware('auth');
