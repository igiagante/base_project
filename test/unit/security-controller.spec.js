'use strict';

const { test, trait } = use('Test/Suite')('Security Controller');
const Env = use('Env');
const Factory = use('Factory');

const api = Env.get('API_BASE_URL');

trait('Test/ApiClient');

test('[/login] should validate request params', async ({ client }) => {
  const response = await client.post(`${api}/security/login`).end();

  response.assertStatus(400);
  response.assertError([
    {
      message: 'required validation failed on email',
      field: 'email',
      validation: 'required',
    },
    {
      message: 'required validation failed on password',
      field: 'password',
      validation: 'required',
    },
  ]);
});

test('[/login] should return invalid credentials', async ({ client }) => {
  const response = await client
    .post(`${api}/security/login`)
    .send({ email: 'test.user@test.com', password: 'fruta' })
    .end();

  response.assertStatus(401);
});

test('[/login] should return the token', async ({ assert, client }) => {
  const password = 'test';
  const user = await Factory.model('App/Models/User').create({ password });

  const response = await client
    .post(`${api}/security/login`)
    .send({ email: user.email, password })
    .end();

  response.assertStatus(200);
  assert.containsAllKeys(response.body, ['token', 'refreshToken', 'type']);
});
