'use strict';

const { test, trait, timeout } = use('Test/Suite')('Image Controller Spec Js');
const Env = use('Env');
const api = Env.get('API_BASE_URL');
const Helpers = use('Helpers');

trait('Test/ApiClient');
timeout(7000);

// test('[/images] should validate image size', async ({ client }) => {
//   const response = await client.post(`${api}/image/upload`).end();

//   response.assertStatus(400);
// });

test('[/images] should upload an image in S3', async ({ client }) => {
  const response = await client
    .post(`${api}/images/upload`)
    .field('title', 'test')
    .attach('images[]', Helpers.tmpPath('chart.png'))
    .end();

  console.log(response.body);
  response.assertStatus(200);
});
