'use strict';

const { test } = use('Test/Suite')('Trade Model');
const Factory = use('Factory');
const Trade = use('App/Models/Trade');

test('should belongs to user', async ({ assert }) => {
  const user = await Factory.model('App/Models/User').create();
  const trade = await Factory.model('App/Models/Trade').create();

  await trade.user().associate(user);

  const result = await Trade.query()
    .where('id', trade.id)
    .with('user')
    .first();

  assert.isObject(result);
  assert.equal(result.toJSON().user.id, user.id);
});
