'use strict';

const { test, trait } = use('Test/Suite')('User Controller Spec Js');
const Env = use('Env');
const Factory = use('Factory');

const api = Env.get('API_BASE_URL');

trait('Test/ApiClient');

test('[/self] should return user information', async ({ client }) => {
  const user = await Factory.model('App/Models/User').create();

  const response = await client
    .get(`${api}/users/self`)
    //    .loginVia(user, 'jwt')
    .end();

  response.assertStatus(200);
  response.assertJSONSubset(user.toJSON());
});
