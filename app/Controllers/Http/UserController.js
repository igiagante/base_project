'use strict';

/**
 * @swagger
 * tags:
 *  - name: users
 *    description: Users apis
 */
class UserController {
  /**
   * @swagger
   * /users/self:
   *  get:
   *    tags:
   *      - users
   *    security:
   *    - Bearer: []
   *    summary: Retrieve user
   *    description: Retrieve user by JWT
   *    consumes:
   *    - application/json
   *    produces:
   *    - application/json
   *    responses:
   *      200:
   *        description: Access tokens
   *      401:
   *        description: Invalid credentials
   */
  async self ({ auth }) {
    const user = await auth.getUser();

    return user;
  }
}

module.exports = UserController;
