'use strict';

const Drive = use('Drive');
const Helpers = use('Helpers');
const uuid = use('uuid/v1');
const Image = use('App/Models/Image');

/**
 * @swagger
 * tags:
 *  - name: images
 *    description: Images apis
 */
class ImageController {
  /**
   * @swagger
   * /images/upload:
   *  get:
   *    tags:
   *      - images
   *    summary: Upload image
   *    description: Upload image in S3
   *    consumes:
   *    - multipart/form-data
   *    produces:
   *    - application/json
   *    parameters:
   *    - name: images
   *      in: formData
   *      required: true
   *      type: file
   *    responses:
   *      200:
   *        description: S3 link to the image file
   *      400:
   *        description: Image too large, you can upload files up to 2 MB
   */
  async upload ({ request, response }) {
    try {
      const validationOptions = {
        types: ['image'],
        size: '2mb',
        extnames: ['png', 'gif'],
      };

      const images = request.file('images', validationOptions);

      if (!images) {
        return response.status(400).send({ message: 'image not received' });
      }

      const fileNames = [];

      await images.moveAll(Helpers.tmpPath('uploads'), file => {
        const name = `${uuid()}.${file.subtype}`;
        fileNames.push(name);
        return { name };
      });

      if (!images.movedAll()) {
        return response.status(400).send(images.errors());
      }

      const uploadToS3 = async fileName => {
        const file = Drive.getStream(Helpers.tmpPath(`uploads/${fileName}`));
        await Drive.disk('s3').put(fileName, file);
        const image = await Image.create({
          fullsize: Drive.disk('s3').getUrl(fileName),
          thumbnail: Drive.disk('s3').getUrl(fileName),
        });
        return image;
      };

      const results = await Promise.all(fileNames.map(uploadToS3));

      return results;
    } catch (error) {
      console.log(error);
      response.status(401).send(error);
    }
  }
}

module.exports = ImageController;
