'use strict';

const { validateAll } = use('Validator');

/**
 * @swagger
 * definitions:
 *   UserCredentials:
 *    type: object
 *    xml:
 *      name: UserCredentials
 *    properties:
 *      required:
 *      - email
 *      - password
 *      email:
 *        type: string
 *      password:
 *        type: string
 * tags:
 *  - name: security
 *    description: Security apis
 */
class SecurityController {
  /**
   * @swagger
   * /security/login:
   *  post:
   *    tags:
   *      - security
   *    summary: Retrieve access tokens
   *    description: Retrieve access tokens by user credentials
   *    consumes:
   *    - application/json
   *    produces:
   *    - application/json
   *    parameters:
   *    - in: body
   *      name: body
   *      description: User credentials
   *      required: true
   *      schema:
   *        $ref: "#/definitions/UserCredentials"
   *    responses:
   *      200:
   *        description: Access tokens
   *      400:
   *        description: Invalid request body
   *      401:
   *        description: Invalid credentials
   */
  async login ({ request, response, auth }) {
    try {
      const rules = {
        email: 'required|string|email',
        password: 'required|string',
      };

      const data = request.only(Object.keys(rules));
      const validation = await validateAll(data, rules);

      if (validation.fails()) {
        return response.status(400).send(validation.messages());
      }

      const result = await auth.withRefreshToken().attempt(data.email, data.password);

      return result;
    } catch (error) {
      response.status(401).send({ message: 'Invalid credentials' });
    }
  }
}

module.exports = SecurityController;
