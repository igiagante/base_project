const Env = use('Env');
const swaggerJSDoc = require('swagger-jsdoc');
const isDevelop = Env.get('NODE_ENV', 'development') === 'development';
const envHost = Env.get('HOST', process.env.HOST);
const envPort = Env.get('PORT', process.env.PORT);
const isLocal = ['localhost', '127.0.0.1'].indexOf(envHost) > -1;

const description =
  'This is the document api for the app Trading strategy its only for testing propouse and development. \n' +
  'To use the apis you have to authenticate first and use the `Bearer ${token}` on each call.';

const schemes = isDevelop && isLocal ? ['http'] : ['https'];

const host = isDevelop && isLocal ? `${envHost}:${envPort}` : envHost;

// swagger definition
const swaggerDefinition = {
  info: {
    title: 'Trading strategy Swagger API',
    version: '1.0.0',
    description,
  },
  host,
  basePath: '/api/v1',
  schemes,
  securityDefinitions: {
    Bearer: {
      type: 'apiKey',
      name: 'Authorization',
      in: 'header',
    },
  },
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./app/Models/*.js', './app/Controllers/Http/*.js'],
};

module.exports = swaggerJSDoc(options);
