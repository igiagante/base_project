'use strict';

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
// const Factory = use('Factory');
const User = use('App/Models/User');
const MOCKS = require('../MOCK_DATA_USERS.json');

class UserSeeder {
  async run () {
    await User.createMany(MOCKS);
  }
}

module.exports = UserSeeder;
