'use strict';

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class AddColumnsFirstAndLastSchema extends Schema {
  up () {
    this.table('users', table => {
      table.string('first_name', 50);
      table.string('last_name', 50);
    });
  }

  down () {
    this.table('users', table => {
      // reverse alternations
      table.dropColumn('first_name');
      table.dropColumn('last_name');
    });
  }
}

module.exports = AddColumnsFirstAndLastSchema;
