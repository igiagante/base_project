'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TradeSchema extends Schema {
  up () {
    this.create('trades', (table) => {
      table.increments()
      table.timestamps()
      // create foreign key with userID
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.float('entry').notNullable()
      table.float('out').notNullable()
    })
  }

  down () {
    this.drop('trades')
  }
}

module.exports = TradeSchema
